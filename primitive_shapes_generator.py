#!/usr/bin/env python3

'''
Code developed by Luca Baronti (lbaronti[at]gmail.com)
'''

import copy, fileinput
import math as mt
import numpy.random as nr

def norm(x):
	return mt.sqrt(sum([pow(p,2) for p in x]))

def normalize_point(x):
	n=norm(x)
	return (x[0]/n, x[1]/n, x[2]/n)

def distance(p1,p2):
	if len(p1)!=len(p2):
		raise ValueError("Asked distance between two points of different size "+str(len(p1))+"!="+str(len(p2)))
	return mt.sqrt(sum([pow(p1[i]-p2[i],2) for i in range(len(p1))]))

def sgn(x):
	if x==0.0:
		return 0
	elif x>0.0:
		return 1
	else:
		return -1

# this is used for float comparison
def mostlyEqual(x,y,threshold=1e-5):
	return abs(x-y)<=threshold

def point2sphericalCoordinate(p):
	radius=norm(p)
	zenith=mt.acos(p[2]/radius)	# phi
	azimuth=mt.atan2(p[1],p[0])	# theta
	return (radius,zenith,azimuth)

# compute an approximation of the volume of the intersecating part of two shapes
# returns the volume both as value and as percentage, relatively to the smallest shape
def intersection_volume(s1,s2,n_samples=int(1.0e3)): # Monte Carlo approach
	count=0.0
	for _ in range(n_samples):
		p, _ = s1.generateRandomPointAndNormal(internal=True)
		if s2.isPointOnSurface(p)!=1: # it is not external
			count+=1
	t1=count/n_samples
	count=0.0
	for _ in range(n_samples):
		p, _ = s2.generateRandomPointAndNormal(internal=True)
		if s1.isPointOnSurface(p)!=1: # it is not external
			count+=1
	t2=count/n_samples
	if t1<t2:
		return s1.getVolume()*t1, t1
	else:
		return s2.getVolume()*t2, t2

class quaternion(object):
	def __init__(self,x=0.0,y=0.0,z=0.0,w=1.0):
		self.x=x
		self.y=y
		self.z=z
		self.w=w

	def conjugate(self):
		return quaternion(-self.x,-self.y,-self.z,self.w)

	def inverse(self): # it should be the same as the conjugate, since it's a unit quaternion
		s=sum([pow(x,2) for x in [self.x, self.y, self.z, self.w]])
		return quaternion(-self.x/s, -self.y/s, -self.z/s, self.w/s)

	def normalize(self):
		v=norm((self.x,self.y,self.z,self.w))
		if v==0: # restore the null quaternion
			self.x=.0
			self.y=.0
			self.z=.0
			self.w=1.0
		else:
			self.x/=v
			self.y/=v
			self.z/=v
			self.w/=v

	def __str__(self):
		return "x="+str(self.x)+" y="+str(self.y)+" z="+str(self.z)+" w="+str(self.w)
		# r, p, y = self.toEuler()
		# return "roll="+str(r)+" pitch="+str(p)+" yaw="+str(y)

	def __mul__(self, rq):
		return quaternion(self.w * rq.x + self.x * rq.w + self.y * rq.z - self.z * rq.y,
		                  self.w * rq.y + self.y * rq.w + self.z * rq.x - self.x * rq.z,
		                  self.w * rq.z + self.z * rq.w + self.x * rq.y - self.y * rq.x,
		                  self.w * rq.w - self.x * rq.x - self.y * rq.y - self.z * rq.z)
	def toEuler(self): # taken from wikipedia
		# roll (x-axis rotation)
		sinr_cosp = 2.0*(self.w * self.x + self.y * self.z)
		cosr_cosp = 1.0 - 2.0 * (self.x * self.x + self.y * self.y)
		roll = mt.atan2(sinr_cosp, cosr_cosp)

		# pitch (y-axis rotation)
		sinp = 2.0 * (self.w * self.y - self.z * self.x)
		if abs(sinp) >= 1:
			pitch = sgn(sinp)*mt.pi/2.0 # use 90 degrees if out of range
		else:
			pitch = mt.asin(sinp)

		# yaw (z-axis rotation)
		siny_cosp = 2.0 * (self.w * self.z + self.x * self.y)
		cosy_cosp = 1.0 - 2.0 * (self.y * self.y + self.z * self.z)
		yaw = mt.atan2(siny_cosp, cosy_cosp)
		return roll, pitch, yaw

	@staticmethod
	def random_quaternion():
		quat=quaternion(nr.uniform(-1,1),nr.uniform(-1,1),nr.uniform(-1,1),nr.uniform(0,1))
		quat.normalize()
		return quat
	
	@staticmethod
	def fromEuler(roll, pitch, yaw, inRadian=True): # rotation on x,y and z axes
		if inRadian:
			p=pitch  / 2.0
			y=yaw / 2.0
			r=roll / 2.0
		else:
			pi_180=mt.pi/180
			p = pitch * pi_180 / 2.0
			y = yaw * pi_180 / 2.0
			r = roll * pi_180 / 2.0
		sinp = mt.sin(p)
		siny = mt.sin(y)
		sinr = mt.sin(r)
		cosp = mt.cos(p)
		cosy = mt.cos(y)
		cosr = mt.cos(r)
		x = sinr * cosp * cosy - cosr * sinp * siny
		y = cosr * sinp * cosy + sinr * cosp * siny
		z = cosr * cosp * siny - sinr * sinp * cosy
		w = cosr * cosp * cosy + sinr * sinp * siny
		q=quaternion(x,y,z,w)
		q.normalize()
		return q

def rotatePoint(p,quat):
	if quat==None:
		return copy.deepcopy(p)
	v_quat=quaternion(p[0],p[1],p[2],0.0)
	res=quat*v_quat*quat.conjugate()
	return (res.x,res.y,res.z)

def translatePoint(p, centre, inverse=False):
	if inverse:
		return (p[0] - centre[0],
						p[1] - centre[1],
						p[2] - centre[2])
	else:
		return (p[0] + centre[0],
						p[1] + centre[1],
						p[2] + centre[2])

def points_dot_prod(p1,p2):
	return sum([p1[i]*p2[i] for i in range(len(p1))])

def pointNnormal2str(point, normal):
	return ' '.join([str(x) for x in point])+' '+' '.join([str(x) for x in normal])

def loadPointCloud(path):
	f=open(path)
	i=0
	ret={"points":[]}
	for line in f.readlines():
		if path.endswith("pcd") and i<11:
			if line.startswith("FIELDS") and "normal" in line:
				ret["normals"]=[]
			i+=1
			continue
		tk=line.split(' ')
		if len(tk)<3:
			raise ValueError("Line "+str(i+1)+" has no point in it: "+line)
		ret["points"]+=[[float(x) for x in tk[:3]]]
		if "normals" in ret:
			if len(tk)<6:
				raise ValueError("Line "+str(i+1)+" has no normals in it (even if it should, according to the header): "+line)
			ret["normals"]+=[[float(x) for x in tk[3:]]]
		i+=1
	f.close()
	return ret

class Shape(object):
	def __init__(self,n_points):
		self.n_points=n_points
		self.centre=(0.0, 0.0, 0.0)
		self.rotation=quaternion() 
		self.error_percentage=0.0
		self.error_absolute=0.0
		self.part_perc=0.0
		self.part_orientation=quaternion()
	
	def __len__(self):
		return self.n_points

	def __str__(self):
		return "Shape centre: "+str(self.centre)

	def translate(self,new_center):
		if len(new_center)!=3:
			raise ValueError("Can't translate the shape, wrong number of point elements ("+str(len(new_center))+")")
		self.centre=new_center

	def rotate(self,quat=None):
		if quat==None: # random rotation as default
			quat=quaternion.random_quaternion()
		self.rotation=quat

	# just an auxiliary funciton
	def rotate_euler(self,pitch, yaw, roll):
		return self.rotate(quaternion.fromEuler(pitch,yaw,roll))

	def set_error(self,error_percentage=0.0,error_absolute=0.0):
		self.error_percentage=error_percentage
		self.error_absolute=error_absolute

	# The higher is part_perc the more shape will be removed. Remove it from left-bottom-up as default
	def remove_part(self, part_perc, remove_from=quaternion.fromEuler(mt.pi/4.0,mt.pi/4.0,mt.pi/4.0)): 
		self.part_perc=part_perc
		self.part_orientation=remove_from

	def _applyPointAndNormal_remove_part(self, point, normal):
		if self.part_perc==0.0:
			return point, normal
		n_tries=0
		p=rotatePoint(point, self.part_orientation) # tilt it a bit
		t, b = self.getBoundingBox().getTopAndBotValues()
		v=(t - b)*self.part_perc + b
		while p[1] < v and n_tries<50: # replace the point
			point, normal = self._generateModifiedRandomPointAndNormal()
			p=rotatePoint(point, self.part_orientation) # tilt it a bit
			n_tries+=1
		return point, normal
		
	def _applyPointAndNormal_add_error(self, point, normal):
		if self.error_percentage==0.0 and self.error_absolute==0.0:
			return point, normal
		bb = self.getBoundingBox()
		min_values, max_values = bb.point_min, bb.point_max
		scene_range=(	max_values[0] - min_values[0],
									max_values[1] - min_values[1],
									max_values[2] - min_values[2])
		deviation=[p*self.error_percentage + self.error_absolute for p in scene_range]	
		point=(point[0]+nr.uniform(-deviation[0],deviation[0]),
					point[1]+nr.uniform(-deviation[1],deviation[1]),
					point[2]+nr.uniform(-deviation[2],deviation[2]))
		norm_dev=2.0*mt.pi*self.error_percentage
		normal=rotatePoint(normal,quaternion.fromEuler(nr.uniform(-norm_dev,+norm_dev),nr.uniform(-norm_dev,+norm_dev),nr.uniform(-norm_dev,+norm_dev)))
		normal=normalize_point(normal) 
		return point, normal

	def _applyPointAndNormal_rotate(self, point, normal):
		point=rotatePoint(point,self.rotation)
		normal=rotatePoint(normal,self.rotation)
		return point, normal

	def _applyPointAndNormal_translate(self, point, normal):
		point=translatePoint(point,self.centre)
		return point, normal

	def _applyPointAndNormal_modifications(self, point, normal):
		point, normal = self._applyPointAndNormal_remove_part(point, normal)
		point, normal = self._applyPointAndNormal_add_error(point, normal)
		point, normal = self._applyPointAndNormal_rotate(point, normal)
		point, normal = self._applyPointAndNormal_translate(point, normal)
		return point, normal

	# this is used only to be consistend in the translation and rotation order of the points
	def _translateAndRotatePoint(self,point, inverse=False):
		if inverse:
			return rotatePoint(translatePoint(point,self.centre,inverse=True),self.rotation.inverse())
		else:
			return translatePoint(rotatePoint(point,self.rotation),self.centre,inverse=False)

	def save(self, path, save_normals=True):
		f=open(path,'w')
		if not path.endswith("xyz"):
			f.write("# .PCD v0.7 - Point Cloud Data file format\n")
			f.write("VERSION 0.7\n")
			if save_normals:
				f.write("FIELDS x y z normal_x normal_y normal_z\n")
				f.write("SIZE 4 4 4 4 4 4\n")
				f.write("TYPE F F F F F F\n")
				f.write("COUNT 1 1 1 1 1 1\n")
			else:
				f.write("FIELDS x y z\n")
				f.write("SIZE 4 4 4\n")
				f.write("TYPE F F F\n")
				f.write("COUNT 1 1 1\n")
			f.write("WIDTH "+str(len(self))+"\n")
			f.write("HEIGHT 1\n")
			f.write("VIEWPOINT 0 0 0 1 0 0 0\n")
			f.write("POINTS "+str(len(self))+"\n")
			f.write("DATA ascii\n")
			self.save_points(f,save_normals)
		else:
			self.save_points(f,save_normals)
		f.close()

	# This function generates all the points at once and return them. 
	# They will not be locally stored, so e.g. every invokations of this function will returns different random points
	def getAllPoints(self, filter_internal_points=[]):
		pointsNnormals=[]
		for i in range(self.n_points):
			point, normal = self.generateRandomPointAndNormal(internal=False)
			if self.is_point_internal_any_shape(point, filter_internal_points):
				continue
			pointsNnormals+=[(point, normal)]
		return pointsNnormals

	def is_point_internal_any_shape(self, point, shapes):
		for s in shapes:
			if s.isPointOnSurface(point)==-1:
				return True
		return False

	# returns the number of points saved
	def save_points(self, file, save_normals=True, filter_internal_points=[]):
		n_points_saved=0
		for i in range(self.n_points):
			point, normal = self.generateRandomPointAndNormal(internal=False)
			if self.is_point_internal_any_shape(point, filter_internal_points):
				continue
			if save_normals:
				file.write(pointNnormal2str(point, normal)+'\n')
			else:
				file.write(pointNnormal2str(point, [])+'\n')
			n_points_saved+=1
		return n_points_saved

	def generateRandomPointAndNormal(self, internal=False):
		point, normal = self._generateModifiedRandomPointAndNormal(internal=internal) # this function must be defined in the subclasses
			# now modify the point and normal
		point, normal = self._applyPointAndNormal_modifications(point, normal)
		return point, normal


class Sphere(Shape):
	def __init__(self,n_points,radius):
		super(Sphere,self).__init__(n_points)
		self.radius=radius
		# self.min_max_height=(-self.radius,self.radius)

	def _generateModifiedRandomPointAndNormal(self, internal=False): # internal==False => generated on surface
		x, y, z=nr.normal(0,1), nr.normal(0,1), nr.normal(0,1)
		s=mt.sqrt(sum([pow(k,2) for k in (x,y,z)]))
		if internal:
			n=self.radius*pow(nr.uniform(0,1),1/3)
		else:
			n=self.radius
		point=(x/s*n, y/s*n,z /s*n)
		normal=(x/s, y/s, z/s)
		return point, normal

	def getBoundingBox(self):
		min_values=[-self.radius]*3
		max_values=[self.radius]*3
		bb = BoundingBox(min_values,max_values)
		bb.translate(self.centre)
		return bb

	def __str__(self):
		return "Sphere centre: "+str(self.centre)+" radius:"+str(self.radius)

	def getVolume(self):
		return 4.0/3.0*mt.pi*pow(self.radius,3)

	def getSurfaceArea(self):
		return 4.0*mt.pi*pow(self.radius,2)

	def getSurfaceDistance(self, point):
		return abs(distance(self.centre, point) - self.radius)

	def isPointOnSurface(self, point):
		d=distance(self.centre, point)
		if mostlyEqual(d,self.radius,threshold=self.radius*0.01):
			return 0
		elif d<self.radius:
			return -1
		else:
			return 1

	@staticmethod
	def csv_labels():
		return "center_x,center_y,center_z,radius"

	def csv(self):
		return ','.join([str(x) for x in list(self.centre)+[self.radius]])

class Box(Shape):
	def __init__(self,n_points,height,width,depth):
		super(Box,self).__init__(n_points)
		self.height=height
		self.width=width
		self.depth=depth

	def _generateModifiedRandomPointAndNormal(self, internal=False): # internal==False => generated on surface
		if internal:
			point=(nr.uniform(-self.width*.5,self.width*.5), nr.uniform(-self.height*.5,self.height*.5), nr.uniform(-self.depth*.5,self.depth*.5))
			if abs(point[0]/self.width*.5)>abs(point[1]/self.height*.5) and abs(point[0]/self.width*.5)>abs(point[2]/self.depth*.5):
				normal = (sgn(point[0]),0,0)
			elif abs(point[1]/self.height*.5)>abs(point[2]/self.depth*.5):
				normal = (0,sgn(point[1]),0)
			else:
				normal = (0,0,sgn(point[2]))
		else:
			face=nr.randint(0,6)
			if face==0:
				point=(-self.width*.5, nr.uniform(-self.height*.5,self.height*.5), nr.uniform(-self.depth*.5,self.depth*.5))
				normal=(-1,0,0)
			elif face==1:
				point=(self.width*.5, nr.uniform(-self.height*.5,self.height*.5), nr.uniform(-self.depth*.5,self.depth*.5))
				normal=(1,0,0)
			elif face==2:
				point=(nr.uniform(-self.width*.5,self.width*.5), -self.height*.5, nr.uniform(-self.depth*.5,self.depth*.5))
				normal=(0,-1,0)
			elif face==3:
				point=(nr.uniform(-self.width*.5,self.width*.5), self.height*.5, nr.uniform(-self.depth*.5,self.depth*.5))
				normal=(0,1,0)
			elif face==4:
				point=(nr.uniform(-self.width*.5,self.width*.5), nr.uniform(-self.height*.5,self.height*.5), -self.depth*.5)
				normal=(0,0,-1)
			else:
				point=(nr.uniform(-self.width*.5,self.width*.5), nr.uniform(-self.height*.5,self.height*.5), self.depth*.5)
				normal=(0,0,1)
		return point, normal

	def __str__(self):
		return "Box centre: "+str(self.centre)+" rotation: ("+str(self.rotation)+") h:"+str(self.height)+" w:"+str(self.width)+" d:"+str(self.depth)

	def getVolume(self):
		return self.width*self.height*self.depth

	def getSurfaceArea(self):
		return 2.0*(self.width*self.height)+2.0*(self.width*self.depth)+2.0*(self.height*self.depth)

	def getFaceCenters(self):
		v=[(-self.width/2.0,0.0,0.0)]
		v+=[(self.width/2.0,0.0,0.0)]
		v+=[(0.0,-self.height/2.0,0.0)]
		v+=[(0.0,self.height/2.0,0.0)]
		v+=[(0.0,0.0,-self.depth/2.0)]
		v+=[(0.0,0.0,self.depth/2.0)]
		return tuple([self._translateAndRotatePoint(p) for p in v])

	def getVertices(self):
		v=[(-self.width/2.0,-self.height/2.0,-self.depth/2.0)]
		v+=[(-self.width/2.0,-self.height/2.0,self.depth/2.0)]
		v+=[(-self.width/2.0,self.height/2.0,-self.depth/2.0)]
		v+=[(-self.width/2.0,self.height/2.0,self.depth/2.0)]
		v+=[(self.width/2.0,-self.height/2.0,-self.depth/2.0)]
		v+=[(self.width/2.0,-self.height/2.0,self.depth/2.0)]
		v+=[(self.width/2.0,self.height/2.0,-self.depth/2.0)]
		v+=[(self.width/2.0,self.height/2.0,self.depth/2.0)]
		return tuple([self._translateAndRotatePoint(p) for p in v])

	def getBoundingBox(self):
		verts = self.getVertices()
		v_min, v_max = [None,None,None], [None,None,None]
		for v in verts:
			for i in [0,1,2]:
				if v_min[i]==None:
					v_min[i]=v[i]
					v_max[i]=v[i]
				else:
					v_min[i]=min(v_min[i],v[i])
					v_max[i]=max(v_max[i],v[i])
		bb = BoundingBox(v_min,v_max)
		return bb

	def getSurfaceDistance(self,point):
		pt=self._translateAndRotatePoint(point,inverse=True)
		x, y, z = pt[0], pt[1], pt[2]
		x_min, x_max = -self.width*.5, self.width*.5
		y_min, y_max = -self.height*.5, self.height*.5
		z_min, z_max = -self.depth*.5, self.depth*.5
		if self.isPointOnSurface(point)<=0:		# the point is internal at the box
			dx=min(x-x_min,x_max-x)
			dy=min(y-y_min,y_max-y)
			dz=min(z-z_min,z_max-z)
			return min( min(dx,dy),dz)
		else:		# the point is external at the box
			dx = max(max(x_min - x, .0), x - x_max)
			dy = max(max(y_min - y, .0), y - y_max)
			dz = max(max(z_min - z, .0), z - z_max)
			return mt.sqrt(pow(dx,2) + pow(dy,2) + pow(dz,2))

	def isPointOnSurface(self, point):
		pt=self._translateAndRotatePoint(point,inverse=True)
		tr=min([self.height,self.depth,self.width])*0.01
		if 	(mostlyEqual(abs(pt[0]),self.width*.5,threshold=tr) and abs(pt[1])<=self.height*.5 and abs(pt[2])<=self.depth*.5) or\
				(mostlyEqual(abs(pt[1]),self.height*.5,threshold=tr) and abs(pt[0])<=self.width*.5 and abs(pt[2])<=self.depth*.5) or\
				(mostlyEqual(abs(pt[2]),self.depth*.5,threshold=tr) and abs(pt[1])<=self.height*.5 and abs(pt[0])<=self.width*.5):
				 return 0
		else:
			if abs(pt[0])<self.width*.5 and abs(pt[1])<self.height*.5 and abs(pt[2])<self.depth*.5:
				return -1
			else:
				return 1

	@staticmethod
	def csv_labels():
		return "center_x,center_y,center_z,qw,qx,qy,qz,height,width,depth"

	def csv(self):
		return ','.join([str(x) for x in list(self.centre)+[self.rotation.w,self.rotation.x,self.rotation.y,self.rotation.z,self.height,self.width,self.depth]])

class Cylinder(Shape):
	def __init__(self,n_points,height,radius):
		super(Cylinder,self).__init__(n_points)
		self.height=height
		self.radius=radius

	def _generateModifiedRandomPointAndNormal(self, internal=False): # internal==False => generated on surface
		face=nr.randint(0,6) # 0 is top and 1 is bottom, the rest is middle
		if face==0:
			x,z=nr.normal(0,1), nr.normal(0,1)
			s=mt.sqrt(sum([pow(k,2) for k in (x,z)]))
			if internal:
				point=(x/s*nr.uniform(0.0,self.radius), nr.uniform(0.0,self.height*.5), z/s*nr.uniform(0.0,self.radius))
			else:
				point=(x/s*nr.uniform(0.0,self.radius), self.height*.5, z/s*nr.uniform(0.0,self.radius))
				normal=(0,1,0)
		elif face==1:
			x,z=nr.normal(0,1), nr.normal(0,1)
			s=mt.sqrt(sum([pow(k,2) for k in (x,z)]))
			if internal:
				point=(x/s*nr.uniform(0.0,self.radius), nr.uniform(-self.height*.5,0.0), z/s*nr.uniform(0.0,self.radius))
			else:
				point=(x/s*nr.uniform(0.0,self.radius), -self.height*.5, z/s*nr.uniform(0.0,self.radius))
				normal=(0,-1,0)
		else:
			x,z=nr.normal(0,1), nr.normal(0,1)
			s=mt.sqrt(sum([pow(k,2) for k in (x,z)]))
			if internal:
				d=self.radius*pow(nr.uniform(0,1),1/3)
				point=(x/s*d, nr.uniform(-self.height*.5,self.height*.5), z/s*d)
			else:
				point=(x/s*self.radius, nr.uniform(-self.height*.5,self.height*.5), z/s*self.radius)
				normal=(x/s, 0.0, z/s)
		if internal: # set the normal
			if abs(point[1]/self.height*.5)>abs(point[0]/self.radius) and abs(point[1]/self.height*.5)>abs(point[2]/self.radius):
				normal = (0,sgn(point[1]),0)
			else:
				s=mt.sqrt(sum([pow(k,2) for k in (point[0],point[2])]))
				normal=(point[0]/s, 0.0, point[2]/s)
		return point, normal
	
	# the problem here is reducing on finding the BB of an unrotated cylinder, rotate it and then returning the BB of that box
	def getBoundingBox(self):
		b=Box(0,width=2*self.radius,depth=2*self.radius,height=self.height)
		b.translate(self.centre)
		b.rotate(self.rotation)
		return b.getBoundingBox()

	def __str__(self):
		return "Shape centre: "+str(self.centre)+" radius:"+str(self.radius)+" height:"+str(self.height)

	def getVolume(self):
		return mt.pi*self.height*pow(self.radius,2)

	def getSurfaceArea(self):
		return 2.0*mt.pi*self.radius*self.height+2.0*mt.pi*pow(self.radius,2)

	def getMainAxis(self):
		bot=(0.0,-self.height/2.0,0.0)
		top=(0.0,self.height/2.0,0.0)
		bot_t=translatePoint(rotatePoint(bot,self.rotation),self.centre)
		top_t=translatePoint(rotatePoint(top,self.rotation),self.centre)
		return (bot_t, top_t)

	def getSurfaceDistance(self,point):
		pt=rotatePoint(translatePoint(point,self.centre,inverse=True), self.rotation.inverse())
		x, y, z = pt[0], pt[1], pt[2]
		x_min, x_max = -self.radius, self.radius
		y_min, y_max = -self.height*.5, self.height*.5
		z_min, z_max = x_min, x_max
		x, y, z = pt[0], pt[1], pt[2]
		if self.isPointOnSurface(point)<=0:
			# the point is internal at the cylinder
			dxz= abs(mt.sqrt(pow(x,2)+pow(z,2))-self.radius)
			dy=self.height*.5-abs(y)
			return min(dxz,dy)
		else:
			# the point is external at the cylinder
			dxz=mt.sqrt(pow(x,2) + pow(z,2))
			if y<y_max and y>y_min:
				# it's internal at the cylinder's height
				ret= abs(mt.sqrt(pow(x,2)+pow(z,2))-self.radius);
			elif dxz<=self.radius:
				# it's internal the cylinder in 2D and external in height
				ret= abs(y) - self.radius*.5
			else:
				# it's completely external at the cylinder
				d_c=dxz - self.radius; # is the distance between the point and the circumference
				y_h=abs(y) - self.radius*.5 # is the height discrepance
				ret= mt.sqrt(pow(d_c,2) + pow(y_h,2))
			return ret

	def isPointOnSurface(self, point):
		pt=self._translateAndRotatePoint(point,inverse=True)
		tr=min([self.height,self.radius])*0.01
		point_r=mt.sqrt(pow(pt[0],2)+pow(pt[2],2))
		if mostlyEqual(abs(pt[1]),self.height*.5,threshold=tr) or (abs(pt[1])<=self.height*.5 and mostlyEqual(point_r,self.radius,threshold=tr)):
			return 0
		elif abs(pt[1])>self.height*.5 or abs(point_r)>self.radius:
			return 1
		else:
			return -1

	@staticmethod
	def csv_labels():
		return "center_x,center_y,center_z,qw,qx,qy,qz,height,radius"

	def csv(self):
		return ','.join([str(x) for x in list(self.centre)+[self.rotation.w,self.rotation.x,self.rotation.y,self.rotation.z,self.height,self.radius]])

# An ausiliary class used only to determine the scene boundaries
class BoundingBox(Box):
	def __init__(self, point_min, point_max):
		w, h, d =point_max[0]-point_min[0], point_max[1]-point_min[1], point_max[2]-point_min[2]
		super(BoundingBox,self).__init__(n_points=0,height=h,width=w,depth=d)
		super(BoundingBox,self).translate([.5*(point_max[i]+point_min[i]) for i in (0,1,2)])
		self.point_min, self.point_max = point_min, point_max

	def __str__(self):
		return "Bounding Box centre:"+str(self.centre)+" h:"+str(self.height)+" w:"+str(self.width)+" d:"+str(self.depth)+" p_min:"+str(self.point_min)+" p_max:"+str(self.point_max)

	def translate(self, point):
		super(BoundingBox,self).translate(point)
		self.point_min = translatePoint(self.point_min, point)
		self.point_max = translatePoint(self.point_max, point)
	# this returns the highest and lowest value on the Y-axis
	def getTopAndBotValues(self):
		return self.point_max[1], self.point_min[1]

	@staticmethod
	def mergeBoundingBoxes(bb1,bb2):
		point_min, point_max = [.0,.0,.0], [.0,.0,.0]
		for i in (0,1,2):
			point_min[i]=min(bb1.point_min[i],bb2.point_min[i])
			point_max[i]=max(bb1.point_max[i],bb2.point_max[i])
		return BoundingBox(point_min, point_max)


# A collection of shapes in the same PC
class Scene(object):
	def __init__(self,initial_shape=None):
		self.shapes=[]
		self.pointsNnormals=None # it's !=None only if the instance is forced to generate statically all the points before saving
		self.n_points=0
		self.bounding_box=BoundingBox([0.0,0.0,0.0],[0.0,0.0,0.0])
		self.n_noisy_points=0
		self.noise_box=None 
		self.noise_enlarge_factor=1.0
		self.generatedPointsOnlyOnSurface=False
		if initial_shape!=None:
			self.add_shape(initial_shape)

	def __len__(self):
		return self.n_points

	def __str__(self):
		ret="Scene\n"
		for s in self.shapes:
			ret+=str(s)+'\n'
		return ret

	def n_shapes(self):
		return len(self.shapes)

	def getVolume(self):
		return (self.scene_max_values[0]-self.scene_min_values[0])*(self.scene_max_values[1]-self.scene_min_values[1])*(self.scene_max_values[2]-self.scene_min_values[2])

	def updateMinMaxPoints(self, point):
		for i in (0,1,2):
			self.scene_max_values[i]=max(self.scene_max_values[i], point[i])
			self.scene_min_values[i]=min(self.scene_min_values[i], point[i])

	def add_shape(self,shape):
		s=copy.deepcopy(shape)
		self.shapes+=[s]
		self.n_points+=len(s)
		self.bounding_box = BoundingBox.mergeBoundingBoxes(self.bounding_box,s.getBoundingBox())

	def _makeNoisyPoint(self):
		if self.noise_box!=None:
			bb = self.noise_box
		else:
			bb = self.bounding_box
		point, _ = bb.generateRandomPointAndNormal(internal=True)
		point = [p*self.noise_enlarge_factor for p in point]
		normal=normalize_point((nr.uniform(0,1),nr.uniform(0,1),nr.uniform(0,1)))
		return point, normal

	def _recomputeBoundingBox(self):
		self.bounding_box=BoundingBox([0.0,0.0,0.0],[0.0,0.0,0.0])
		for s in self.shapes:
			self.bounding_box=BoundingBox.mergeBoundingBoxes(self.bounding_box,s.getBoundingBox())

	def add_noise(self,n_noisy_points,enlarging_boundaries_factor=1.0, custom_noise_box=None):
		self.noise_box=custom_noise_box
		self.n_noisy_points+=n_noisy_points
		self.noise_enlarge_factor=enlarging_boundaries_factor

	def rotate(self, quat):
		[s.rotate(quat) for s in self.shapes]
		self._recomputeBoundingBox()

	def set_error(self,error_percentage=0.0,error_absolute=0.0):
		[s.set_error(error_percentage,error_absolute) for s in self.shapes]
	
	def getRandomPoints(self,random_perc=.1):
		n_points_tot=self.n_noisy_points + self.n_points
		ret_points, ret_normals = [], []
		for i in range(int(n_points_tot*random_perc)):
			if self.pointsNnormals!=None:
				p, n = nr.choice(self.pointsNnormals)
			else:
				toss=nr.uniform(0,n_points_tot)
				if toss<self.n_noisy_points:
					p, n = self._makeNoisyPoint()
				else:
					s=nr.choice(self.shapes)
					p, n = s.generateRandomPointAndNormal()
			ret_points+=[p]
			ret_normals+=[n]
		return ret_points, ret_normals

	# this should NOT be called unless you want to generate all the points at once and store all of them in memory
	def generateAllPointsAndNormal(self, only_surface=False):
		self.pointsNnormals=[]
		for s in self.shapes:
			if only_surface:
				self.pointsNnormals+=s.getAllPoints(self.shapes)
			else:
				self.pointsNnormals+=s.getAllPoints()
		for _ in range(self.n_noisy_points):
			p, n = self._makeNoisyPoint()
			if only_surface and len(self.shapes)>0 and self.shapes[0].is_point_internal_any_shape(p, self.shapes):
				continue
			self.pointsNnormals+=[(p,n)]
		self.generatedPointsOnlyOnSurface=only_surface
		return self.pointsNnormals

	def save(self, path, save_normals=True, only_surface=False):
		f=open(path,'w')
		if path.endswith("xyz"):
			pc_format="xyz"
		else:
			pc_format="pcd"
		if pc_format=="pcd":
			f.write("# .PCD v0.7 - Point Cloud Data file format\n")
			f.write("VERSION 0.7\n")
			if save_normals:
				f.write("FIELDS x y z normal_x normal_y normal_z\n")
				f.write("SIZE 4 4 4 4 4 4\n")
				f.write("TYPE F F F F F F\n")
				f.write("COUNT 1 1 1 1 1 1\n")
			else:
				f.write("FIELDS x y z\n")
				f.write("SIZE 4 4 4\n")
				f.write("TYPE F F F\n")
				f.write("COUNT 1 1 1\n")
			f.write("WIDTH "+str(self.n_points + self.n_noisy_points)+"\n")
			f.write("HEIGHT 1\n")
			f.write("VIEWPOINT 0 0 0 1 0 0 0\n")
			f.write("POINTS "+str(self.n_points + self.n_noisy_points)+"\n")
			f.write("DATA ascii\n")
		n_points_saved=0 # used in case of only_surface==True
		if self.pointsNnormals!=None:
			for i in range(len(self.pointsNnormals)):
				if only_surface and not self.generatedPointsOnlyOnSurface and len(self.shapes)>0 and self.shapes[0].is_point_internal_any_shape(self.pointsNnormals[i][0], self.shapes):
					continue
				if save_normals:
					f.write(pointNnormal2str(self.pointsNnormals[i][0], self.pointsNnormals[i][1])+'\n')
				else:	
					f.write(pointNnormal2str(self.pointsNnormals[i][0], [])+'\n')
				n_points_saved+=1
		else:
			for s in self.shapes:
				if only_surface:
					n_points_saved+=s.save_points(f, save_normals=save_normals, filter_internal_points=self.shapes)
				else:
					n_points_saved+=s.save_points(f, save_normals=save_normals)
			for _ in range(self.n_noisy_points):
				p, n = self._makeNoisyPoint()
				if only_surface and len(self.shapes)>0 and self.shapes[0].is_point_internal_any_shape(p, self.shapes):
					continue
				if not save_normals:
					n=[]
				f.write(pointNnormal2str(p, n)+'\n')
				n_points_saved+=1
		f.close()
		# if only surface points are generated, the number of points in the PCD file need to be updated
		if (only_surface or self.generatedPointsOnlyOnSurface) and pc_format=='pcd':
			f=fileinput.input(path, inplace=1)
			for line in f:
				if line.startswith("WIDTH"):
					print("WIDTH "+str(n_points_saved))
				elif line.startswith("POINTS"):
					print("POINTS "+str(n_points_saved))
				else:
					print(line.strip())
