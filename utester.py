#!/usr/bin/env python3

import primitive_shapes_generator as pg
import random as rn
import unittest

class TestSphere(unittest.TestCase):
	def test_pointsGeneration(self):
		s = pg.Sphere(1000,radius=rn.uniform(0.1,10))
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

	def test_pointsGenerationTranslated(self):
		s = pg.Sphere(1000,radius=rn.uniform(0.1,10))
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

class TestBox(unittest.TestCase):
	def test_pointsGeneration(self):
		s = pg.Box(1000,depth=1,width=4,height=9)
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

	def test_pointsGenerationTranslated(self):
		s = pg.Box(1000,depth=1,width=4,height=9)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

	def test_pointsGenerationTranslatedAndRotated(self):
		s = pg.Box(1000,depth=1,width=4,height=9)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		s.rotate()
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)


class TestCylinder(unittest.TestCase):
	def test_pointsGeneration(self):
		s = pg.Cylinder(1000,radius=1.5,height=5.76)
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

	def test_pointsGenerationTranslated(self):
		s = pg.Cylinder(1000,radius=1.5,height=5.76)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

	def test_pointsGenerationTranslatedAndRotated(self):
		s = pg.Cylinder(1000,radius=1.5,height=5.76)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		s.rotate()
		points = s.getAllPoints()
		for p, n in points:
			self.assertEqual(s.isPointOnSurface(p),0)

class TestInternalPointsGeneration(unittest.TestCase):

	def testSphere(self):
		n_points=1000
		s=pg.Sphere(1,rn.uniform(0.1,10))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
	
	def testBox(self):
		n_points=1000
		s=pg.Box(1,rn.uniform(0.1,10),rn.uniform(0.1,10),rn.uniform(0.1,10))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
		s.rotate()
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
	
	def testCylinder(self):
		n_points=1000
		s=pg.Cylinder(1,rn.uniform(0.1,10),rn.uniform(0.1,10))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
		s.translate((rn.uniform(-10,10),rn.uniform(-10,10),rn.uniform(-10,10)))
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)
		s.rotate()
		for _ in range(n_points):
			p, _ = s.generateRandomPointAndNormal(internal=True)
			self.assertTrue(s.isPointOnSurface(p)<1)

'''
class TestVolume(unittest.TestCase):
	def testShapeInShapeVolumeIntersection(self):
		s1=pg.Sphere(1,radius=2)
		s2=pg.Sphere(1,radius=1)
		v, p = pg.intersection_volume(s1,s2)
		self.assertEqual(v,s2.getVolume())
	
	def test_volumeIntersectionSphere(self):
		s1=pg.Sphere(1000,radius=1)
		s2=pg.Sphere(1,radius=1)
		v, p = pg.intersection_volume(s1,s2)
		self.assertEqual(p,1.0)
		s2.translate((1,0,0))
		v, p = pg.intersection_volume(s1,s2)
		self.assertTrue(p<1.0 and p>0.0)
		s2.translate((5,0,0))
		v, p = pg.intersection_volume(s1,s2)
		self.assertEqual(p,0.0)
		self.assertEqual(v,0.0)
		b=pg.Box(1000,2,2,2)
		b.translate((1,0,0))
		v, p = pg.intersection_volume(s1,b)
		sc=pg.Scene(s1)
		sc.add_shape(b)
		sc.save("/tmp/prova.pcd")
		print(s1.getVolume(),b.getVolume())
		self.assertEqual(v,.5*s1.getVolume())
	def test_volumeIntersectionBox(self):
		s1=pg.Box(1000,1,1,1)
		s2=pg.Box(1,1,1,1)
		v, p = pg.intersection_volume(s1,s2)
		self.assertEqual(p,1.0)
		s2.translate((.5,0,0))
		v, p = pg.intersection_volume(s1,s2,n_samples=10000)
		self.assertEqual(p,.5)
		self.assertEqual(v,.5*s1.getVolume())
		s2.translate((5,0,0))
		v, p = pg.intersection_volume(s1,s2)
		self.assertEqual(p,0.0)
		self.assertEqual(v,0.0)
'''

if __name__ == '__main__':
  unittest.main()
